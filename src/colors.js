/* @flow */

function color(r,g,b): string {
  return `rgb(${r}, ${g}, ${b});`;
}

var COLORS: Array<string> = [
  color(176, 37, 33),
  color(199, 63, 5),
  color(122, 43, 157),
  color(41, 79, 95),
  color(63, 45, 33),
  color(125, 165, 26),
  color(44, 57, 109),
  color(63, 45, 33),
];

module.exports = {
  all: COLORS,
  pick: (idx: number): string => COLORS[idx % COLORS.length],
};
