/* @flow */
'use strict';

var React = require('react-native');
var Parse = require('parse').Parse;
var colors = require('./colors');
var {
  AppRegistry,
  StyleSheet,
  Image,
  Text,
  View,
} = React;

// TASK #2. Change text color. Add some margin between text and image.
// TASK #3. Position the content in the center
// TASK #4: Make image tappable (e.g. TouchableOpacity)
// TASK #5: Uncomment the query in `componentDidMount`
// TASK #6. Render text of the first secret. You can put it in place of 'Hello'
// TASK #7. Render a list of secrets. This one is a bit more complex. For every
//          secret you want to render its own <Text />
// TASK #8. Extract secret view into its own component.
// TASK #9. Make the list scrollable.
// TASK #9. Add a composer, send some secrets to Parse

class SecretApp extends React.Component {
  constructor() {
    super();
    this.state = { secrets: [] };
  }

  render() {
    return (
      <View style={styles.container}>
        <Image
          style={styles.foxImage}
          source={{uri: 'http://i.imgur.com/bmlBvRN.png'}}
        />
        <View style={{marginTop: 20}}>
          <Text style={styles.helloText}>Hello, fox!</Text>
        </View>
      </View>
    );
  }

  componentDidMount() {
    // See https://parse.com/docs/js_guide#queries-basic
    // Parse initialization and 'Secret' object definition are at the end of this file
    // Extra credit - sort secrets so the newest are at the top
    //
    // var query = new Parse.Query(Secret);
    // query.find({
    //   success: (list) => this.handleSecretsLoaded(list),
    //   error: (error) => console.error(error),
    // });
  }

  handleSecretsLoaded(secretsList: Array<Secret>) {
    // Make handleSecretsLoaded function store secrets in this component's state
    // http://facebook.github.io/react/docs/component-api.html#setstate
    // `console` output is sent to `adb logcat`
    // You can remove this line.
    console.log(secretsList);
  }
}

var IMAGE_SIZE = 100;

var styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(35, 16, 11)',
    // TASK #2 Hint: Add some styles here
  },
  helloText: {
    color: 'white',
  },
  foxImage: {
    borderRadius: IMAGE_SIZE / 2,
    width: IMAGE_SIZE,
    height: IMAGE_SIZE,
  },
});


// Some helper methods and Parse initialization
Parse.initialize(
  'tZAFpUNPbEFenoFRE1hgiTZ5HYKHmpFLiaDaqnsI',
  'S0xhwVHSBPgJkl6z8GXHaCBNc0HoHxiR7dwPHRVs'
);
var Secret = Parse.Object.extend('Secret');

module.exports = SecretApp;
