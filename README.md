# React Native Secret App

Instructions:

1. `brew install node`
2. `npm install -g react-native-cli`
3. `git clone https://bitbucket.org/frantic/secret-app.git`
4. `cd secret-app`
5. `react-native run-android`

![](screenshot.png)
